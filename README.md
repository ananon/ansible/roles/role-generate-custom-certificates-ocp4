# Generate Openshift Custom Certificates

This role generates openshift certificates from rootCA in github (https://github.com/matancarmeli7/cloudlet/tree/master/ca).
This role will generate for you crt and key files for apps and api for your cluster.

## add to you inventory the following parameters:
--------------------------------------------------------------------------------

* **cluster_name:** your cluster name
* **ocp_web:** our apache server fqdn(snir-installer-rhel.centralus.cloudapp.azure.com)
* **openshift_custom_certificates:** true